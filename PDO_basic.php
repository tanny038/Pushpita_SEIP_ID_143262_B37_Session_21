<?php
print_r(PDO::getAvailableDrivers());

$host = "localhost";
$dbName = "Atomic_Project_B37";
$user = "root";
$pass = "";

try {
    $DBH = new PDO("mysql:host=$host;dbname=$dbName", $user, $pass);
    // set the PDO error mode to exception
    $DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


    $STH = $DBH->prepare("select * from book_title");
    $STH->execute();
    echo "Connected successfully" ."<br/>";

    $sql = "DELETE FROM book_title WHERE id=6";

    // use exec() because no results are returned
    $DBH->exec($sql);
    echo "Record deleted successfully";

}

catch(PDOException $e){

    echo "I'm sorry, Dave. I'm afraid I can't do that." . "<br/>";

    file_put_contents('PDOErrors.txt', $e->getMessage(), FILE_APPEND);

}

/*$data = array('Book_Name' =>'PHP Version 3', 'Authors_Name' =>'Hamidur Rahman');


$STH1 = $DBH->prepare("insert into book_title (Book_Name,Authors_Name) value (:Book_Name, :Authors_Name)");

$STH1->execute($data);*/




$STH->setFetchMode(PDO::FETCH_ASSOC);
while($row = $STH->fetch()){

    echo "<br/>" . $row['ID'] . "<br/>";

    echo $row['Book_Name'] . "<br/>";

    echo $row['Authors_Name'] . "<br/>";

}

/*catch(PDOException $e)
{
    echo "Connection failed: " . $e->getMessage();
}*/
$STH->execute();

$STH->setFetchMode(PDO::FETCH_OBJ);
while($row = $STH->fetch()){

    echo "<br/>" . $row->ID . "<br/>";

    echo $row->Book_Name . "<br/>";

    echo $row->Authors_Name . "<br/>";

}




?>